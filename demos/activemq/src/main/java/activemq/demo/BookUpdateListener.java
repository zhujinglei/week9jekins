package activemq.demo;

import activemq.demo.json.event.BookChangedEvent;

public class BookUpdateListener {
  public void bookUpdated(BookChangedEvent event) {
    System.out.println("Book with isbn: " + event.getIsbn() + " has changed.");
  }
}
