package activemq.demo.json.event;

import java.util.Objects;

public class BookChangedEvent {
  private long isbn;

  public BookChangedEvent() {
  }

  public BookChangedEvent(long isbn) {
    this.isbn = isbn;
  }

  public long getIsbn() {
    return isbn;
  }

  public void setIsbn(long isbn) {
    this.isbn = isbn;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BookChangedEvent that = (BookChangedEvent) o;
    return isbn == that.isbn;
  }

  @Override
  public int hashCode() {
    return Objects.hash(isbn);
  }
}
