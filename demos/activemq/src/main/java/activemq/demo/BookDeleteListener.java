package activemq.demo;

import activemq.demo.json.event.BookChangedEvent;

public class BookDeleteListener {
  public void bookDeleted(BookChangedEvent event) {
    System.out.println("Book with isbn: " + event.getIsbn() + " has been deleted.");
  }
}
