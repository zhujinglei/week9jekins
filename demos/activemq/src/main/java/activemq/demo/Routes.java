package activemq.demo;
import activemq.demo.json.event.BookChangedEvent;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Routes extends RouteBuilder {
  @Value("${incoming.book.delete.uri}")
  private String bookDeleteUri;

  @Value("${incoming.book.update.uri}")
  private String bookUpdateUri;

  @Override
  public void configure() throws Exception {
    from(bookUpdateUri)
        .unmarshal().json(JsonLibrary.Jackson, BookChangedEvent.class)
        .bean(new BookUpdateListener(), "bookUpdated");

    from(bookDeleteUri)
        .unmarshal().json(JsonLibrary.Jackson, BookChangedEvent.class)
        .bean(new BookDeleteListener(), "bookDeleted");
  }
}