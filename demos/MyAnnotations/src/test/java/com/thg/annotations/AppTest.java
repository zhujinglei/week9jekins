package com.thg.annotations;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;


public class AppTest 
{
  App obj;


  @Before
  public void setUp() throws Exception {
       obj= new App();
  }

  @Test
  public void checkFlag() {
      boolean value= obj.checkFlag();
      assertFalse(value);
  }

  @Test
  public void checkFlagAgain() {
    boolean value= obj.checkFlagAgain();
    assertFalse(value);
  }
}
