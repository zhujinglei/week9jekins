package com.thg.annotations;

public class Autoboxing {

  public static void main(String[] args) {
    int i = 10;
    Integer a = i;
    System.out.println("Autoboxing from i to a " + a);

    int firstInteger = 1;
    int lastInteger = 1;
    if (firstInteger == lastInteger) {
      System.out.println("Autoboxing working");
    } else {
      System.out.println("Both are not equal");
    }

    Integer firstIntegerAgain = 1;
    Integer lastIntegerAgain = 1;
    if (firstIntegerAgain == lastIntegerAgain) {
      System.out.println("Autoboxing working");
    } else {
      System.out.println("Both are not equal");
    }
    //"==" operator it compares object's identity and not value and also no auto boxing occur.
    Integer first = new Integer(1);
    Integer last = new Integer(1);
    if (first == last) {
      System.out.println("Autoboxing working");
    } else {
      System.out.println("Both are not equal");
    }

    //primitive does not suppose to have null value
   //Integer myValue=null;
   //int ii=myValue+77;


  }
}


