package com.thg.annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class App {
  boolean flag = false;


  public static void main(String[] args) throws NoSuchFieldException {
    Class<UseAnnotation> obj = UseAnnotation.class;
    Class<UseAnnotationTwo> obj1 = UseAnnotationTwo.class;
    Annotation annotation = obj.getAnnotation(MyClassLevelAnnotation.class);
    Annotation annotation1 = obj1.getAnnotation(MyClassLevelAnnotation.class);


    System.out.println("priority " + ((MyClassLevelAnnotation) annotation).priority());
    System.out.println("created by  " + ((MyClassLevelAnnotation) annotation).createdBy());
    System.out.println("last modified  " + ((MyClassLevelAnnotation) annotation).lastModified());
    System.out.println("______________________________________");
    System.out.println("priority " + ((MyClassLevelAnnotation) annotation1).priority());
    System.out.println("created by  " + ((MyClassLevelAnnotation) annotation1).createdBy());
    System.out.println("last modified  " + ((MyClassLevelAnnotation) annotation1).lastModified());
    System.out.println("______________________________________");
    // How to capture method annotations of a class by using java reflection

    Method[] methods = UseMethodAnnotation.class.getMethods();
    for (Method myMethods : methods) {
      if (myMethods.isAnnotationPresent(MethodAnnotations.class)) {
        MethodAnnotations getValues = myMethods.getAnnotation(MethodAnnotations.class);
        System.out.println(getValues.name());
        System.out.println(getValues.departmentID());
        System.out.println("______________________________________");
      }
    }


  }

  public boolean checkFlag() {
    if (flag == true) {
      return flag;
    }

    return false;

  }

  public boolean checkFlagAgain() {
    if (flag == false) {
      return flag;
    }

    return true;

  }

}
