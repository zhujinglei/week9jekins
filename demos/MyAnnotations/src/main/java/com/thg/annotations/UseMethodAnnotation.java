package com.thg.annotations;


import java.lang.annotation.Annotation;

public class UseMethodAnnotation {

  @MethodAnnotations(name = "Nathen Drake", departmentID = "ITA123")
  public void applyAnnotations(){

  }

  @MethodAnnotations()
  public void applyDefaultAnnotations(){

  }




}
