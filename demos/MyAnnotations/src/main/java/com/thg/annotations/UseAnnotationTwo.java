package com.thg.annotations;

@MyClassLevelAnnotation(
        priority = MyClassLevelAnnotation.Priority.LOW,
        createdBy = "james bond",
        lastModified = "07/03/1996")
public class UseAnnotationTwo {
}
