package com.thg.annotations;

import com.thg.annotations.MyClassLevelAnnotation;

@MyClassLevelAnnotation(
        priority = MyClassLevelAnnotation.Priority.HIGH,
        createdBy = "khan",
        lastModified = "07/03/1999")
public class UseAnnotation {
}
