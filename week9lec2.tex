\documentclass{beamer}
 
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{color}
\makeatletter
\setlength\beamer@paperwidth{16.00cm}%
\setlength\beamer@paperheight{9.00cm}%
\geometry{papersize={\beamer@paperwidth,\beamer@paperheight}}
\makeatother
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\usepackage{graphicx}
\graphicspath{ {./images/} }

\lstset{ 
	backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
	basicstyle=\footnotesize,        % the size of the fonts that are used for the code
	breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
	breaklines=true,                 % sets automatic line breaking
	captionpos=b,                    % sets the caption-position to bottom
	commentstyle=\color{mygreen},    % comment style
	deletekeywords={...},            % if you want to delete keywords from the given language
	escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
	extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
	frame=single,	                   % adds a frame around the code
	keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
	keywordstyle=\color{blue},       % keyword style
	language=Octave,                 % the language of the code
	morekeywords={*,...},            % if you want to add more keywords to the set
	numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
	numbersep=5pt,                   % how far the line-numbers are from the code
	numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
	rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
	showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=false,          % underline spaces within strings only
	showtabs=false,                  % show tabs within strings adding particular underscores
	stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
	stringstyle=\color{mymauve},     % string literal style
	tabsize=2,	                   % sets default tabsize to 2 spaces
	title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}
 
%Information to be included in the title page:
\title{Java Course}
\subtitle{Week 9, Lecture 2: ActiveMQ}
\author{Shaun Hall/Rehman Arshad}
\institute{THG Accelerator}
\date{2019}
 
 
 
\begin{document}
 \lstset{language=Java}
\frame{\titlepage}



\begin{frame}
\frametitle{Software Architecture Recap}
	\centering
\includegraphics[width=0.46\linewidth]{images/pasta}

\end{frame}

\begin{frame}
\frametitle{High-level Code Structure Recap}
\centering
\includegraphics[width=1\linewidth]{images/rest-app-structure}

\end{frame}

\begin{frame}
\frametitle{Code Location}
Code examples for this lecture are \href{https://gitlab.io.thehut.local/Accelerator18/course-material/java-course/tree/master/week8/demos/rest-demo}{here (producer)} and \href{https://gitlab.io.thehut.local/Accelerator18/course-material/java-course/tree/master/week9/demos/activemq}{here (consumer)} 
\end{frame}

\begin{frame}
\frametitle{Messaging Service Motivation}
The REST architecture is a data pull model -- clients get information when they ask for it. Some domains require a push data model where services actively notify interested parties when something changes (e.g. order placement).

\vspace{1cm}

Ideally, producers aren't aware of who the receivers are (encapsulation). This means we need a \textbf{messaging service} in between the two.

\vspace{1cm}

There are two popular messaging patterns -- \textbf{message queue} and \textbf{pub/sub}.
\end{frame}

\begin{frame}
\frametitle{Message Queue Model}
The service emitting data puts the message onto a named \textbf{queue}. The queue has FIFO semantics and the receiving service pulls messages from the queue.

\vspace{1cm}

When a message is pulled by a \textbf{worker}, it is removed from the queue. 

\vspace{1cm}


This model is \textbf{persistent}, so if a subscriber has a period of downtime, messages emitted during this period will be available when it restarts.

\vspace{1cm}


The model suits domains where a single piece of work needs doing for each message.

\end{frame}

\begin{frame}
\frametitle{Message Queue Diagram}
\centering
\includegraphics[width=0.9\linewidth]{images/queues}

\end{frame}

\begin{frame}
\frametitle{Pub/Sub Model}

The service sending data is called a \textbf{publisher} and interested parties are called \textbf{subscribers}.

\vspace{1cm}

Subscribers are interested in a subset of total messages published. The messaging middleware provides a \textbf{filtering service} to select relevant messages.

\vspace{1cm}

\textbf{Topics} are named logical channels used to implement filtering. Publishers emit messages onto topics, and subscribers receive messages from all topics they have expressed an interest in.

\vspace{1cm}

The pub/sub model is \textbf{not persistent}, so if a subscriber has a period of downtime, messages emitted during this period will not be available when it restarts.


\end{frame}

\begin{frame}
\frametitle{Pub/Sub Diagram}
	\centering
\includegraphics[width=0.9\linewidth]{images/topics}

\end{frame}



\begin{frame}
\frametitle{Apache ActiveMQ}
ActiveMQ is an open-source messaging service that supports both the message queue and pub/sub models.

\vspace{1cm}

It also supports a combination of the two models -- combining the message filtering of pub-sub with a persistent worker queue.

\vspace{1cm}

At THG, we use the JMS (Java Messaging Service) protocol to interact with the activemq broker.

\end{frame}

\begin{frame}
\frametitle{ActiveMQ -- Combining Topics and Queues}
	\centering
\includegraphics[width=0.55\linewidth]{images/amq-topics-queues}

\end{frame}

\begin{frame}
\frametitle{Java Code}
ActiveMQ is just one possible messaging service, so ideally our code shouldn't be coupled to ActiveMQ.

\vspace{1cm}

We use Apache Camel as an abstraction layer for message routing -- it supports an enormous number of protocols (\href{http://camel.apache.org/components.html}{link}).

\vspace{1cm}

Code examples  \href{https://gitlab.io.thehut.local/Accelerator18/course-material/java-course/tree/master/week8/demos/rest-demo}{here (producer)} and \href{https://gitlab.io.thehut.local/Accelerator18/course-material/java-course/tree/master/week9/demos/activemq}{here (consumer)}.


\end{frame}

\begin{frame}
\frametitle{Design Decisions}
\begin{itemize}
\item How descriptive should your events be?
\item Do you use topics or message content to convey what has changed?
\item Do you include all the data that has changed in your message or just a reference to the entity?
\end{itemize}

\end{frame}

\end{document}