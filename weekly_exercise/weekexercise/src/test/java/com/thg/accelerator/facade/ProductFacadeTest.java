package com.thg.accelerator.facade;

import com.palantir.docker.compose.DockerComposeRule;
import com.thg.accelerator.json.ProductJson;
import com.thg.accelerator.repository.impl.PostgresProductRepositryTest;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ProductFacadeTest extends PostgresProductRepositryTest {

  @ClassRule
  public static DockerComposeRule docker = DockerComposeRule.builder()
      .file("/Users/zhuj/javalecture/java-course/week9/weekly_exercise/weekexercise/docker-composetest.yaml")
      .saveLogsTo("/Users/zhuj/javalecture/java-course/week9/weekly_exercise/weekexercise/src/test/resources")
      .build();

  @Autowired
  private WebTestClient webTestClient;

  @Test
  public void getAllProducts() {
    ProductJson productJson = new ProductJson(0, "A", 1.1, "C", "D", "E");
    webTestClient.post().uri("/product/newproduct").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(productJson)).exchange()
        .expectStatus().isCreated();
    ProductJson productJson1 = new ProductJson(1, "A", 1.1, "C", "D", "E");
    webTestClient.post().uri("/product/newproduct").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(productJson1)).exchange()
        .expectStatus().isCreated();
    webTestClient.get().uri("/product").exchange().expectStatus().isOk().expectBody(String.class);
    webTestClient.delete().uri("/product/0").exchange().expectStatus().isNoContent();
    webTestClient.delete().uri("/product/1").exchange().expectStatus().isNoContent();
    webTestClient.get().uri("/product").exchange().expectStatus().isOk();
  }

  @Test
  public void getProduct() {
    ProductJson productJson = new ProductJson(0, "A", 1.1, "C", "D", "E");
    webTestClient.post().uri("/product/newproduct").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(productJson)).exchange()
        .expectStatus().isCreated();
    webTestClient.get().uri("/product/0").exchange().expectStatus().isOk().expectBody(ProductJson.class).isEqualTo(productJson);
    webTestClient.delete().uri("/product/0").exchange().expectStatus().isNoContent();
  }

  @Test
  public void deleteProduct() {
    ProductJson productJson = new ProductJson(0, "A", 1.1, "C", "D", "E");
    webTestClient.post().uri("/product/newproduct").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(productJson)).exchange()
        .expectStatus().isCreated();

    //webTestClient.get().uri("/product/1").exchange().expectStatus().isOk().expectBody(ProductJson.class).isEqualTo(productJson);
    webTestClient.delete().uri("/product/0").exchange().expectStatus().isNoContent().expectBody().isEmpty();
    webTestClient.get().uri("/product/0").exchange().expectStatus().is5xxServerError();

  }

  @Test
  public void updateProduct() {
    ProductJson productJson = new ProductJson(0, "A", 1.1, "C", "D", "E");
    webTestClient.post().uri("/product/newproduct").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(productJson)).exchange()
        .expectStatus().isCreated();
    webTestClient.get().uri("/product/0").exchange().expectStatus().isOk();
    ProductJson productJsonUpgraded = new ProductJson(0, "B", 1.9, "C", "D", "E");
    webTestClient.put().uri("product/0").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(productJsonUpgraded)).exchange()
        .expectStatus().isNoContent();
    webTestClient.get().uri("product/0").exchange().expectStatus().isOk().expectBody(ProductJson.class).isEqualTo(productJsonUpgraded);
    webTestClient.delete().uri("product/0").exchange().expectStatus().isNoContent();
  }

  @Test
  public void search() {
    ProductJson productJson = new ProductJson(0, "A", 1.1, "C", "D", "E");
    webTestClient.post().uri("/product/newproduct").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(productJson)).exchange()
        .expectStatus().isCreated();
    ProductJson productJson1 = new ProductJson(1, "A", 1.1, "C", "D", "E");
    webTestClient.post().uri("/product/newproduct").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(productJson1)).exchange()
        .expectStatus().isCreated();
    webTestClient.get().uri("/product").exchange().expectStatus().isOk().expectBody(String.class);

    webTestClient.post().uri("product/search/A").exchange().expectStatus().isOk();
    webTestClient.delete().uri("/product/0").exchange().expectStatus().isNoContent();
    webTestClient.delete().uri("/product/1").exchange().expectStatus().isNoContent();
    webTestClient.get().uri("/product").exchange().expectStatus().isOk();
  }

  @Test
  public void createProduct() {

    ProductJson productJson = new ProductJson(0, "A", 1.1, "C", "D", "E");
    webTestClient.post().uri("/product/newproduct").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(productJson)).exchange()
        .expectStatus().isCreated();
    webTestClient.get().uri("/product/0").exchange().expectStatus().isOk().expectBody(ProductJson.class).isEqualTo(productJson);
    webTestClient.delete().uri("/product/0").exchange().expectStatus().isNoContent();
  }
}