package com.thg.accelerator.service;

import com.thg.accelerator.model.Product;
import com.thg.accelerator.repository.ProductRepository;
import com.thg.accelerator.repository.RepositoryException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@TestExecutionListeners(inheritListeners = false, listeners = {
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class })
@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {
  @Mock
  private ProductRepository productRepository;

  @InjectMocks
  private ProductService productService;

  Product myTestProduct = new Product.ProductBuilder().productID(1).name("a").price(1.1).category("c").description("d").imageURL("e").createProduct();
  Product MyTestProduct1 = new Product.ProductBuilder().productID(1).name("a").price(1.1).category("c").description("d").imageURL("e").createProduct();

  @Test
  public void updateProduct() throws RepositoryException, ServiceException {
    Mockito.doNothing().when(productRepository).updateProduct(myTestProduct);
    productService.updateProduct(myTestProduct);
    verify(productRepository,times(1)).updateProduct(myTestProduct);
  }

  @Test
  public void createProduct() throws RepositoryException, ServiceException {
    Mockito.doReturn(1).when(productRepository).createProduct(myTestProduct);
    productService.createProduct(myTestProduct);
    verify(productRepository, times(1)).createProduct(myTestProduct);
    // verify(productService,times(1)).createProduct(myTestProduct);
  }

  @Test
  public void deleteProduct() throws RepositoryException, ServiceException {
    Mockito.doNothing().when(productRepository).deleteProduct(0);
    productService.deleteProduct(0);
    verify(productRepository, times(1)).deleteProduct(0);
  }

  @Test
  public void findProducts() throws RepositoryException, ServiceException {
    when(productRepository.getProduct((int) myTestProduct.getProductID())).thenReturn(Optional.of(myTestProduct));
    assertEquals(myTestProduct, productService.getProduct((int) myTestProduct.getProductID()).get());
    verify(productRepository, times(1)).getProduct((int) myTestProduct.getProductID());
  }

  @Test
  public void findAllProducts() throws RepositoryException, ServiceException {
    Product myTestProduct1 = new Product.ProductBuilder().productID(1).name("a").price(1.1).category("c").description("d").imageURL("e").createProduct();
    Product myTestProduct2 = new Product.ProductBuilder().productID(1).name("a").price(1.1).category("c").description("d").imageURL("e").createProduct();
    List<Product> mylist = new ArrayList<>();
    mylist.add(myTestProduct1);
    mylist.add(myTestProduct2);
    when(productRepository.findAllProducts()).thenReturn(mylist);
    assertEquals(2, productService.findAllProducts().size());
    verify(productRepository, times(1)).findAllProducts();
  }

  @Test
  public void getProduct() throws RepositoryException, ServiceException {
    when(productRepository.getProduct((int) myTestProduct.getProductID())).thenReturn(Optional.of(myTestProduct));
    assertEquals(myTestProduct, productService.getProduct((int) myTestProduct.getProductID()).get());
    verify(productRepository, times(1)).getProduct((int) myTestProduct.getProductID());
  }

  @Test(expected = ServiceException.class)
  public void ShouldThrowExceptionGetProduct() throws RepositoryException, ServiceException {
    doThrow(RepositoryException.class).when(productRepository).getProduct(100);
    productService.getProduct(100);
  }

  @Test(expected = ServiceException.class)
  public void ShouldThrowExceptionDeletProduct() throws RepositoryException, ServiceException {
    doThrow(RepositoryException.class).when(productRepository).deleteProduct(1);
    productService.deleteProduct(1);
  }

  @Test(expected = ServiceException.class)
  public void ShouldThrowExceptionFindallProduct() throws RepositoryException, ServiceException {
    doThrow(RepositoryException.class).when(productRepository).findAllProducts();
    productService.findAllProducts();
  }

  @Test(expected = ServiceException.class)
  public void ShouldThrowExceptionCreateProduct() throws RepositoryException, ServiceException {
    doThrow(RepositoryException.class).when(productRepository).createProduct(myTestProduct);
    productService.createProduct(myTestProduct);
  }

  @Test(expected = ServiceException.class)
  public void ShouldThrowExceptionUpgradeProduct() throws RepositoryException,ServiceException {
    doThrow(RepositoryException.class).when(productRepository).updateProduct(myTestProduct);
    productService.updateProduct(myTestProduct);
  }

  @Test(expected = ServiceException.class)
  public void ShouldThrowExceptionFindProducts() throws RepositoryException,ServiceException{
    doThrow(RepositoryException.class).when(productRepository).findProducts("a");
    productService.findProducts("a");
  }



  

}