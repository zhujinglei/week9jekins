package com.thg.accelerator.repository.impl;

import com.palantir.docker.compose.DockerComposeRule;
import com.thg.accelerator.model.Product;
import com.thg.accelerator.repository.ProductRepository;
import com.thg.accelerator.repository.RepositoryException;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestExecutionListeners(inheritListeners = false, listeners = {
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class })
public class PostgresProductRepositryTest {
  @ClassRule
  public static DockerComposeRule docker = DockerComposeRule.builder()
      .file("/Users/zhuj/javalecture/java-course/week9/weekly_exercise/weekexercise/docker-composetest.yaml")
      .saveLogsTo("/Users/zhuj/javalecture/java-course/week9/weekly_exercise/weekexercise/src/test/resources")
      .build();

  @Autowired
  private ProductRepository productRepository;


  @Test
  public void createProduct() throws RepositoryException {
    Product product = new Product.ProductBuilder().productID().name("hi").price(1.0).category("home").description("very good for test")
        .imageURL("www.googletest.com").createProduct();
    productRepository.createProduct(product);
    Optional<Product> testProduct = productRepository.getProduct(0);
    Assert.assertEquals(product, testProduct.get());
    System.out.println(product.toString());
    productRepository.deleteProduct(0);
  }

  @Test
  public void testIncrementalID() throws RepositoryException, InterruptedException {

    Product product0 = new Product.ProductBuilder().name("hi").price(1.0).category("home").description("very good for test")
        .imageURL("www.googletest.com").createProduct();
    productRepository.createProduct(product0);

    Product product1 = new Product.ProductBuilder().name("hi").price(1.0).category("home").description("very good for test")
        .imageURL("www.googletest.com").createProduct();
    productRepository.createProduct(product1);
    Optional<Product> testProduct0 = productRepository.getProduct(0);
    Optional<Product> testProduct1 = productRepository.getProduct(1);
    System.out.println(testProduct0.toString() + testProduct1.toString());
    Assert.assertEquals(testProduct0.get().getProductID(), testProduct1.get().getProductID() - 1);
    productRepository.deleteProduct(0);
    productRepository.deleteProduct(1);
  }

  @Test
  public void updateProduct() throws RepositoryException, InterruptedException {

    Product product0 = new Product.ProductBuilder().name("hi").price(1.0).category("home").description("very good for test")
        .imageURL("www.googletest.com").createProduct();
    productRepository.createProduct(product0);

    Product updatedProduct = new Product.ProductBuilder().productID(0)
        .name("hi").price(18.0).category("homeoh").description("very bad for test")
        .imageURL("www.googletest.com").createProduct();
    productRepository.updateProduct(updatedProduct);
    Optional<Product> productupdate = productRepository.getProduct(0);
    Assert.assertEquals(productupdate.get(), updatedProduct);
    productRepository.deleteProduct(0);
  }


  @Test
  public void deleteProduct() throws RepositoryException, SQLException {

    Product product0 = new Product.ProductBuilder().name("hi").price(1.0).category("home").description("very good for test")
        .imageURL("www.googletest.com").createProduct();
    productRepository.createProduct(product0);
    System.out.println("the product is " + product0.toString());
    productRepository.deleteProduct(0);
    Optional<Product> testProduct0 = productRepository.getProduct(0);
    Assert.assertEquals(testProduct0, Optional.empty());
  }

  @Test
  public void findProducts() throws RepositoryException, SQLException {
    Product product1 = new Product.ProductBuilder()
        .name("hi").price(1.0).category("home").description("very good for test")
        .imageURL("www.googletest.com").createProduct();

    Product product2 = new Product.ProductBuilder()
        .name("hi").price(1.0).category("home").description("very good for test")
        .imageURL("www.googletest.com").createProduct();

    productRepository.createProduct(product1);
    productRepository.createProduct(product2);

    List<Product> mysearchingresult = productRepository.findProducts("good");
    for (Product i : mysearchingresult) {
      System.out.println(i.toString());
    }
    Assert.assertEquals(2, mysearchingresult.size());
    productRepository.deleteProduct(1);
    productRepository.deleteProduct(0);
  }

//  @Test
//  public void getProduct() throws RepositoryException, SQLException {
//    Product product = new Product.ProductBuilder().productID().name("hi").price(1.0).category("home").description("very good for test")
//        .imageURL("www.googletest.com").createProduct();
//    productRepository.createProduct(product);
//    Optional<Product> testProduct = productRepository.getProduct(0);
//    Assert.assertEquals(product, testProduct.get());
//    System.out.println(product.toString());
//  }

  @Test
  public void findAllProducts() throws RepositoryException, SQLException {
    Product product1 = new Product.ProductBuilder()
        .name("hi").price(1.0).category("home").description("very good for test")
        .imageURL("www.googletest.com").createProduct();

    Product product2 = new Product.ProductBuilder()
        .name("hi").price(1.0).category("home").description("very good for test")
        .imageURL("www.googletest.com").createProduct();

    Product product3 = new Product.ProductBuilder()
        .name("hi").price(1.0).category("home").description("very good for test")
        .imageURL("www.googletest.com").createProduct();

    Product product4 = new Product.ProductBuilder()
        .name("hi").price(1.0).category("home").description("very good for test")
        .imageURL("www.googletest.com").createProduct();

    productRepository.createProduct(product1);
    productRepository.createProduct(product2);
    productRepository.createProduct(product4);
    productRepository.createProduct(product3);

    List<Product> mysearchingresult = productRepository.findAllProducts();
    for (Product i : mysearchingresult) {
      System.out.println(i.toString());
    }
    Assert.assertEquals(4, mysearchingresult.size());
    productRepository.deleteProduct(0);
    productRepository.deleteProduct(1);
    productRepository.deleteProduct(2);
    productRepository.deleteProduct(3);

  }
}