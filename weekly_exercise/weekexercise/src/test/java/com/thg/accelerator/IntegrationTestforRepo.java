//package com.thg.accelerator;
//
//
//import com.palantir.docker.compose.DockerComposeRule;
//import com.thg.accelerator.model.Product;
//import com.thg.accelerator.repository.ProductRepository;
//import com.thg.accelerator.repository.RepositoryException;
//import org.junit.Assert;
//import org.junit.ClassRule;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.TestExecutionListeners;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
//import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
//
//import java.sql.SQLException;
//import java.util.Optional;
//
//@SpringBootTest
//@RunWith(SpringRunner.class)
//@TestExecutionListeners(inheritListeners = false, listeners = {
//    DependencyInjectionTestExecutionListener.class,
//    DirtiesContextTestExecutionListener.class })
//public class IntegrationTestforRepo {
//  @ClassRule
//  public static DockerComposeRule docker = DockerComposeRule.builder()
//      .file("/Users/zhuj/javalecture/java-course/week9/weekly_exercise/weekexercise/docker-composetest.yaml")
//      .saveLogsTo("/Users/zhuj/javalecture/java-course/week9/weekly_exercise/weekexercise/src/test/resources")
//      .build();
//
//  @Autowired
//  private ProductRepository productRepository;
//
//
//  @Test
//  public void testCreateProduct() throws RepositoryException {
//    Product product = new Product.ProductBuilder().productID().name("hi").price(1.0).category("home").description("very good for test")
//        .imageURL("www.googletest.com").createProduct();
//    productRepository.createProduct(product);
//    Optional<Product> testProduct = productRepository.getProduct(0);
//    Assert.assertEquals(product, testProduct.get());
//    System.out.println(product.toString());
//  }
//
//  @Test
//  public void testIncrementalID() throws RepositoryException, InterruptedException {
//
//    Product product0 = new Product.ProductBuilder().name("hi").price(1.0).category("home").description("very good for test")
//        .imageURL("www.googletest.com").createProduct();
//    productRepository.createProduct(product0);
//
//    Product product1 = new Product.ProductBuilder().name("hi").price(1.0).category("home").description("very good for test")
//        .imageURL("www.googletest.com").createProduct();
//    productRepository.createProduct(product1);
//    Optional<Product> testProduct0 = productRepository.getProduct(0);
//    Optional<Product> testProduct1 = productRepository.getProduct(1);
//    System.out.println(testProduct0.toString() + testProduct1.toString());
//    Assert.assertEquals(testProduct0.get().getProductID(), testProduct1.get().getProductID() - 1);
//  }
//
//  @Test
//  public void testDeleteProduct() throws RepositoryException, SQLException {
//    Product product0 = new Product.ProductBuilder().name("hi").price(1.0).category("home").description("very good for test")
//        .imageURL("www.googletest.com").createProduct();
//    productRepository.createProduct(product0);
//    System.out.println("the product is " + product0.toString());
//    productRepository.deleteProduct(0);
//    Optional<Product> testProduct0 = productRepository.getProduct(0);
//    Assert.assertEquals(testProduct0,Optional.empty());
//  }
//
//
////  @Test
////  public void testDeletProduct() throws RepositoryException, SQLException {
////  }
//
//
//}
