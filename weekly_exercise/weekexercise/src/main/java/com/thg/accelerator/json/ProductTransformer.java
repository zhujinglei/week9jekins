package com.thg.accelerator.json;

import com.thg.accelerator.model.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductTransformer {
  public ProductJson toJson(Product product) {

    return new ProductJson(product.getProductID(), product.getName(),
        product.getPrice(), product.getCategory(), product.getDescription(), product.getImageURL());

  }

  public Product fromJson(ProductJson productJson) {
    return new Product.ProductBuilder().productID(productJson.getProductID())
        .name(productJson.getName()).price(productJson.getPrice())
        .category(productJson.getCategory())
        .description(productJson.getDescription())
        .imageURL(productJson.getImageURL()).createProduct();
  }

  public Product fromJson(NewProductJason newProductJason) {
    return new Product.ProductBuilder().name(newProductJason.getName())
        .price(newProductJason.getPrice()).category(newProductJason.getCategory())
        .description(newProductJason.getDescription())
        .imageURL(newProductJason.getImageURL()).createProduct();
  }

}
