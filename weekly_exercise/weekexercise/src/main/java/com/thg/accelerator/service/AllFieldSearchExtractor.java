package com.thg.accelerator.service;

import com.thg.accelerator.model.Product;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AllFieldSearchExtractor implements Function<Product, String> {

  @Override
  public String apply(Product product) {
    return product.getProductID() + product.getName() + product.getPrice()
        + product.getCategory() + product.getDescription() + product.getImageURL();
  }
}
