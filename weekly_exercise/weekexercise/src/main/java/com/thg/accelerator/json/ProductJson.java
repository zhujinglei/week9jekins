package com.thg.accelerator.json;

public class ProductJson {
  private int productID;
  private String name;
  private double price;
  private String category;
  private String description;
  private String imageURL;

  public ProductJson(int productID, String name, double price, String category,
                     String description, String imageURL) {
    this.productID = productID;
    this.name = name;
    this.price = price;
    this.category = category;
    this.description = description;
    this.imageURL = imageURL;
  }

  public int getProductID() {
    return productID;
  }

  public String getName() {
    return name;
  }

  public double getPrice() {
    return price;
  }

  public String getCategory() {
    return category;
  }

  public String getDescription() {
    return description;
  }

  public String getImageURL() {
    return imageURL;
  }

  public void setProductID(int productID) {
    this.productID = productID;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setImageURL(String imageURL) {
    this.imageURL = imageURL;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ProductJson that = (ProductJson) o;

    if (productID != that.productID) {
      return false;
    }
    if (Double.compare(that.price, price) != 0) {
      return false;
    }
    if (name != null ? !name.equals(that.name) : that.name != null) {
      return false;
    }
    if (category != null ? !category.equals(that.category) : that.category != null) {
      return false;
    }
    if (description != null ? !description.equals(that.description) : that.description != null) {
      return false;
    }
    return imageURL != null ? imageURL.equals(that.imageURL) : that.imageURL == null;
  }

  @Override
  public int hashCode() {
    int result;
    long temp;
    result = (int) (productID ^ (productID >>> 32));
    result = 31 * result + (name != null ? name.hashCode() : 0);
    temp = Double.doubleToLongBits(price);
    result = 31 * result + (int) (temp ^ (temp >>> 32));
    result = 31 * result + (category != null ? category.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (imageURL != null ? imageURL.hashCode() : 0);
    return result;
  }
}
