package com.thg.accelerator.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class Product {
  private int productID;
  private String name;
  private double price;
  private String category;
  private String description;
  private String imageURL;

  private Product(int productID, String name, double price, String category, String description, String imageURL) {
    this.productID = productID;
    this.name = name;
    this.price = price;
    this.category = category;
    this.description = description;
    this.imageURL = imageURL;
  }

  public int getProductID() {
    return productID;
  }

  public String getName() {
    return name;
  }

  public double getPrice() {
    return price;
  }

  public String getCategory() {
    return category;
  }

  public String getDescription() {
    return description;
  }

  public String getImageURL() {
    return imageURL;
  }

  public String toString() {
    return "Product{" + "ProductID= " + productID + ", product name= " + name
        + ", price=" + price + ", category=" + category + ", description= " + description + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Product product = (Product) o;

    if (productID != product.productID) {
      return false;
    }
    if (Double.compare(product.price, price) != 0) {
      return false;
    }
    if (name != null ? !name.equals(product.name) : product.name != null) {
      return false;
    }
    if (category != null ? !category.equals(product.category) : product.category != null) {
      return false;
    }
    if (description != null ? !description.equals(product.description)
        : product.description != null) {
      return false;
    }
    return imageURL != null ? imageURL.equals(product.imageURL) : product.imageURL == null;
  }

  @Override
  public int hashCode() {
    int result;
    long temp;
    result = (productID ^ (productID >>> 32));
    result = 31 * result + (name != null ? name.hashCode() : 0);
    temp = Double.doubleToLongBits(price);
    result = 31 * result + (int) (temp ^ (temp >>> 32));
    result = 31 * result + (category != null ? category.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (imageURL != null ? imageURL.hashCode() : 0);
    return result;
  }

  public static class ProductBuilder {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private int productID;
    private String name;
    private double price;
    private String category;
    private String description;
    private String imageURL;


    public ProductBuilder productID() {
      try {
        int rs = jdbcTemplate.queryForObject("SELECT MAX(productid) "
            + "FROM productinfo ", Integer.class) + 1;
        this.productID = rs;
        return this;
      } catch (NullPointerException e) {
        this.productID = 0;
        return this;
      }
    }

    public ProductBuilder productID(int id) {
      this.productID = id;
      return this;
    }

    public ProductBuilder name(String name) {
      this.name = name;
      return this;
    }

    public ProductBuilder price(double price) {
      this.price = price;
      return this;
    }

    public ProductBuilder category(String category) {
      this.category = category;
      return this;
    }

    public ProductBuilder description(String description) {
      this.description = description;
      return this;
    }

    public ProductBuilder imageURL(String imageURL) {
      this.imageURL = imageURL;
      return this;
    }

    public Product createProduct() {
      return new Product(productID, name, price, category, description, imageURL);
    }
  }
}
