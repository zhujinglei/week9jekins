package com.thg.accelerator.service;


import com.thg.accelerator.model.Product;
import com.thg.accelerator.repository.ProductRepository;
import com.thg.accelerator.repository.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Component
public class ProductService {
  @Autowired
  private ProductRepository productRepository;
  @Autowired
  private Function<Product, String> fieldSearchExtractor;

  public void updateProduct(Product product) throws ServiceException {
    try {
      productRepository.updateProduct(product);
    } catch (RepositoryException ex) {
      throw new ServiceException("Error while updating the product with ID"
          + product.getProductID(), ex);
    }
  }

  public int createProduct(Product product) throws ServiceException {
    try {
      return productRepository.createProduct(product);

    } catch (RepositoryException ex) {
      throw new ServiceException("Error while creating the product", ex);
    }
  }

  public void deleteProduct(int id) throws ServiceException {
    try {
      productRepository.deleteProduct(id);
    } catch (RepositoryException ex) {
      throw new ServiceException("Error while deleting the product with the ID " + id, ex);
    }
  }

  public List<Product> findProducts(String search) throws ServiceException {
    try {
      return productRepository.findProducts(search);
    } catch (RepositoryException ex) {
      throw new ServiceException("Error while searching for " + search, ex);
    }

  }

  public List<Product> findAllProducts() throws ServiceException {
    try {
      return productRepository.findAllProducts();
    } catch (RepositoryException ex) {
      throw new ServiceException("Error while retrieving all the product information", ex);
    }
  }

  public Optional<Product> getProduct(int id) throws ServiceException {
    try {
      return productRepository.getProduct(id);
    } catch (RepositoryException ex) {
      throw new ServiceException("Get a repository error while "
          + "search for product with ID " + id, ex);
    }
  }
}
