package com.thg.accelerator.json;

public class NewProductJason {
  private String name;
  private double price;
  private String category;
  private String description;
  private String imageURL;

  public NewProductJason() {

  }

  public NewProductJason(String name, double price,
                         String category, String description, String imageURL) {
    this.name = name;
    this.price = price;
    this.category = category;
    this.description = description;
    this.imageURL = imageURL;
  }

  public String getName() {
    return name;
  }

  public double getPrice() {
    return price;
  }

  public String getCategory() {
    return category;
  }

  public String getDescription() {
    return description;
  }

  public String getImageURL() {
    return imageURL;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    NewProductJason that = (NewProductJason) o;

    if (Double.compare(that.price, price) != 0) {
      return false;
    }
    if (name != null ? !name.equals(that.name) : that.name != null) {
      return false;
    }
    if (category != null ? !category.equals(that.category) : that.category != null) {
      return false;
    }
    if (description != null ? !description.equals(that.description) : that.description != null) {
      return false;
    }
    return imageURL != null ? imageURL.equals(that.imageURL) : that.imageURL == null;
  }

  @Override
  public int hashCode() {
    int result;
    long temp;
    result = name != null ? name.hashCode() : 0;
    temp = Double.doubleToLongBits(price);
    result = 31 * result + (int) (temp ^ (temp >>> 32));
    result = 31 * result + (category != null ? category.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (imageURL != null ? imageURL.hashCode() : 0);
    return result;
  }
}
