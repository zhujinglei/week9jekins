package com.thg.accelerator.facade;

import com.thg.accelerator.json.ErrorJson;
import com.thg.accelerator.json.NewProductJason;
import com.thg.accelerator.json.ProductJson;
import com.thg.accelerator.json.ProductTransformer;
import com.thg.accelerator.model.Product;
import com.thg.accelerator.service.ProductService;
import com.thg.accelerator.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/product")

public class ProductFacade {
  @Autowired
  private ProductTransformer productTransformer;

  @Autowired
  private ProductService productService;


  @RequestMapping(value = "", method = RequestMethod.GET)
  public ResponseEntity<List<String>> getAllProducts() throws ServiceException {
    try {
      List<Product> products = productService.findAllProducts();
      List<String> resources = new ArrayList<>();
      for (Product product : products) {
        ControllerLinkBuilder getLinks = ControllerLinkBuilder.linkTo(methodOn(
            this.getClass()).getProduct((int) product.getProductID()));
        resources.add("Product with ID " + product.getProductID() + " : " + getLinks.toString());
      }
      return new ResponseEntity<>(resources, HttpStatus.OK);
    } catch (ServiceException ex) {
      System.out.println("There are some problems happened ! ");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public org.springframework.hateoas.Resource<ProductJson> getProduct(@PathVariable int id) {
    try {
      Optional<Product> product = productService.getProduct(id);
      ProductJson productJson = productTransformer.toJson(product.get());
      org.springframework.hateoas.Resource<ProductJson> productJsonResource;
      productJsonResource = new org.springframework.hateoas.Resource<>(productJson);
      // ControllerLinkBuilder myLinkself =
      // ControllerLinkBuilder.linkTo(methodOn(this.getClass()).getProduct(id));
      ControllerLinkBuilder myLinkparent;
      myLinkparent = ControllerLinkBuilder.linkTo(methodOn(this.getClass()).getAllProducts());
      // productJsonResource.add(myLinkself.withSelfRel());
      productJsonResource.add(myLinkparent.withRel("parents"));
      return productJsonResource;
    } catch (ServiceException ex) {
      System.out.println("There is no product with such ID" + id);
      // org.springframework.hateoas.Resource<ProductJson>
      // productJsonResource = new org.springframework.hateoas.Resource<>();
      // return HttpStatus.NOT_FOUND;
    }
    return null;
  }


  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<Void> deleteProduct(@PathVariable(value = "id") int id)
      throws ServiceException {
    productService.deleteProduct(id);
    Optional<Product> product = productService.getProduct(id);
    if (product.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
  public ResponseEntity<ErrorJson> updateProduct(@PathVariable(value = "id") int id, @RequestBody ProductJson productJson)
      throws ServiceException {
    if (id != productJson.getProductID()) {
      return new ResponseEntity<>(new ErrorJson("ID in the path "
          + "didn't match id in product, check again"), HttpStatus.BAD_REQUEST);
    }
    Product product = productTransformer.fromJson(productJson);
    productService.updateProduct(product);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }


  @RequestMapping(value = "/search/{query}", method = RequestMethod.POST)
  public ResponseEntity<List<ProductJson>>
  search(@PathVariable(value = "query") String query) throws ServiceException {
    List<Product> products = productService.findProducts(query);
    List<ProductJson> productJsons1 = new ArrayList<>();
    for (Product product : products) {
      productJsons1.add(productTransformer.toJson(product));
    }
    //List<ProductJson> productJsons = products.stream()
    // .map(productTransformer::toJson).collect(Collectors.toList());
    return new ResponseEntity<>(productJsons1, HttpStatus.OK);
  }

  @RequestMapping(value = "/newproduct", method = RequestMethod.POST)
  ResponseEntity<String> createProduct(@RequestBody NewProductJason newProductJason)
      throws ServiceException {
    Product createdProduct = productTransformer.fromJson(newProductJason);
    try {
      int id = productService.createProduct(createdProduct);
      ControllerLinkBuilder myLink;
      myLink = ControllerLinkBuilder.linkTo(methodOn(this.getClass()).getProduct(id));
      //URI location = ServletUriComponentsBuilder.fromCurrentRequest()
      // .path("/{id}").buildAndExpand(id).toUri();
      //ResponseEntity.created(location).build();
      return new ResponseEntity<>(" A new Product with ID " + id + " in:  "
          + myLink.toString(), HttpStatus.CREATED);
    } catch (ServiceException ex) {
      System.out.println("Cannot up create such product!");
      return ResponseEntity.noContent().build();
    }
  }

  /*
  public org.springframework.hateoas.Resource<ProductJson> getProductById(@PathVariable int id) {
    try {
      Optional<Product> product = productService.getProduct(id);
      ProductJson productJson = productTransformer.toJson(product.get());
      org.springframework.hateoas.Resource<ProductJson>
      productJsonResource = new org.springframework.hateoas.Resource<>(productJson);
      ControllerLinkBuilder myLink =
      ControllerLinkBuilder.linkTo(methodOn(this.getClass()).getProduct(id));
      //ControllerLinkBuilder myLink1 =
      ControllerLinkBuilder.linkTo(methodOn(this.getClass()).getTaskById(name, id));
      productJsonResource.add(myLink.withRel("parent"));
      //myTasks.add(myLink1.withSelfRel());
      return productJsonResource;
    } catch (ServiceException e) {
      System.out.println("There is no product with such ID" + id);
    }
    return null;
  } */
}



