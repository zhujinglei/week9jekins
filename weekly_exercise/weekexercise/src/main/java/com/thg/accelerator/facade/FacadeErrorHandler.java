package com.thg.accelerator.facade;


import com.thg.accelerator.json.ErrorJson;
import com.thg.accelerator.service.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class FacadeErrorHandler {
  @ExceptionHandler(ServiceException.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public ErrorJson processError(ServiceException ex) {
    return new ErrorJson("There has been an error servicing your request. "
        + "Please ask the development team to check the logs.");
  }
}
