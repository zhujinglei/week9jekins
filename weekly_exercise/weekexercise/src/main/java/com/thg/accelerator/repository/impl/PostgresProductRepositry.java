package com.thg.accelerator.repository.impl;

import com.thg.accelerator.model.Product;
import com.thg.accelerator.repository.ProductRepository;
import com.thg.accelerator.repository.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class PostgresProductRepositry implements ProductRepository {
  @Autowired
  private JdbcTemplate jdbcTemplate;

  //Spring 对数据库操做 进行了deep encapsulation and use Spring injection which injected the data templates;
  //test pass

  // the null pointer must be changed when i got time!!!!!!!!! look!
  // sychronize can be optimal
  @Override
  public synchronized int createProduct(Product product) throws RepositoryException {
    try {
      if (jdbcTemplate.getMaxRows() != 0) {

        Integer rs = jdbcTemplate.queryForObject(
            "SELECT MAX(productid) FROM productinfo ", Integer.class) + 1;

        jdbcTemplate.update(
            "insert into productinfo (productid, name, price, category, desription, image_url) "
                + "values (?, ?, ?, ?, ?, ?)",
            rs, product.getName(), product.getPrice(), product.getCategory(),
            product.getDescription(), product.getImageURL());
        return rs;
      } else {
        int rs = 0;
        jdbcTemplate.update(
            "insert into productinfo (productid, name, price, category, desription, image_url) "
                + "values (?, ?, ?, ?, ?, ?)",
            rs, product.getName(), product.getPrice(), product.getCategory(),
            product.getDescription(), product.getImageURL());
        return rs;

      }
    } catch (NullPointerException ex) {
      int rs = 0;
      jdbcTemplate.update(
          "insert into productinfo (productid, name, price, category, desription, image_url) "
              + "values (?, ?, ?, ?, ?, ?)",
          rs, product.getName(), product.getPrice(), product.getCategory(),
          product.getDescription(), product.getImageURL());
      return rs;
    } catch (DataAccessException ex) {
      throw new RepositoryException("failed to insert book", ex);
    }
  }

  @Override
  public void updateProduct(Product product) throws RepositoryException {

    String sql = "update productinfo set name=?, price=?, category=?,desription=?,image_url=? "
        + "where productID=?";
    try {
      jdbcTemplate.update(sql, product.getName(), product.getPrice(), product.getCategory(),
          product.getDescription(), product.getImageURL(), product.getProductID());
    } catch (DataAccessException ex) {
      throw new RepositoryException("Fail to update product", ex);
    }
  }

  // test pass
  @Override
  public void deleteProduct(int productID) throws RepositoryException {
    try {
      jdbcTemplate.update("DELETE FROM productinfo WHERE productid=?", productID);
    } catch (DataAccessException ex) {
      throw new RepositoryException("Fail to delete product with productID:" + productID);
    }
  }

  // test fail!!
  @Override
  public List<Product> findProducts(String search) throws RepositoryException {
    try {
      String sql = "SELECT * from productinfo where desription "
          + "like CONCAT('%', CONCAT(?, '%')) OR category like CONCAT('%', CONCAT(?, '%')) ";

      List<Product> products = jdbcTemplate.query(sql,
          (resultSet, i) -> {
            int productID = resultSet.getInt("productID");
            String name = resultSet.getString("name");
            double price = resultSet.getDouble("price");
            String category = resultSet.getString("category");
            String description = resultSet.getString("desription");
            String imageurl = resultSet.getString("image_url");
            return new Product.ProductBuilder().productID(productID)
                .name(name)
                .price(price)
                .category(category)
                .description(description)
                .imageURL(imageurl).createProduct();
          }, search, search);
      return products;
    } catch (DataAccessException ex) {
      throw new RepositoryException("fail to query for books", ex);
    }
  }

  @Override
  public Optional<Product> getProduct(int productID) throws RepositoryException {
    try {
      return jdbcTemplate.queryForObject("SELECT * from productinfo where productID=?",
          (resultSet, i) -> {
            //the lambada expression shows how to map the sql row with the java world
            //book repositry is the bridge!
            String name = resultSet.getString("name");
            double price = resultSet.getDouble("price");
            String category = resultSet.getString("category");
            String desription = resultSet.getString("desription");
            String imageurl = resultSet.getString("image_url");
            return Optional.of(new Product.ProductBuilder().productID(productID)
                .name(name)
                .price(price)
                .category(category)
                .description(desription)
                .imageURL(imageurl).createProduct());
          }, productID);
    } catch (EmptyResultDataAccessException ex) {
      return Optional.empty();
    } catch (DataAccessException ez) {
      throw new RepositoryException("Fail to query for the product", ez);
    }
  }

  @Override
  public List<Product> findAllProducts() throws RepositoryException {
    try {
      String sql = "SELECT * from productinfo ";

      List<Product> products = jdbcTemplate.query(sql,
          (resultSet, i) -> {
            int productID = resultSet.getInt("productID");
            String name = resultSet.getString("name");
            double price = resultSet.getDouble("price");
            String category = resultSet.getString("category");
            String description = resultSet.getString("desription");
            String imageurl = resultSet.getString("image_url");
            return new Product.ProductBuilder().productID(productID)
                .name(name)
                .price(price)
                .category(category)
                .description(description)
                .imageURL(imageurl).createProduct();
          });
      return products;
    } catch (DataAccessException ex) {
      throw new RepositoryException("fail to query for books", ex);
    }
  }
}


//  public void upateProduct2(Product product) throws RepositoryException {
//
//    String sql = "update productinfo set name=?, price=?,
//    category=?,desription=?,image_url=? where productID=?";
//    try {
//      jdbcTemplate.update(sql, new Object[]
//      {product.getName(), product.getPrice(), product.getCategory(),
//          product.getDescription(), product.getImageURL(), product.getProductID()});
//    } catch (DataAccessException e) {
//      throw new RepositoryException("Fail to update book", e);
//    }
//  }

//  //some problem with the test!
////  @Override
////  public void updateProduct(Product product) throws RepositoryException {
////    SqlParameterSource nameParameters = new MapSqlParameterSource()
////        .addValue("productID", product.getProductID())
////        .addValue("name", product.getName())
////        .addValue("price", product.getPrice())
////        .addValue("category", product.getCategory())
////        .addValue("desription", product.getDescription())
////        .addValue("image_url", product.getImageURL());
////    try {
////      jdbcTemplate.update("update productinfo set name=:name,
// price=:price, category=:category, desription=:desription, image_url=:image_url
// where productid=:productID ", nameParameters);
////    } catch (DataAccessException e) {
////      throw new RepositoryException("fail to update book", e);
////    }
////  }
