package com.thg.accelerator.repository;

import com.thg.accelerator.model.Product;

import java.util.List;

import java.util.Optional;

public interface ProductRepository {

  int createProduct(Product product) throws RepositoryException;

  void updateProduct(Product product) throws RepositoryException;

  void deleteProduct(int productID) throws RepositoryException;

  List<Product> findProducts(String search) throws RepositoryException;

  List<Product> findAllProducts() throws RepositoryException;

  Optional<Product> getProduct(int productID) throws RepositoryException;


}
