create database products;
\connect products;

CREATE TABLE productinfo (
 productID   BIGINT PRIMARY KEY,
 name VARCHAR (50) NOT NULL,
 price NUMERIC (5,2) NOT NULL,
 category varchar (355) NOT NULL,
 desription VARCHAR (5000) NOT NULL,
 image_URL VARCHAR (5000) NOT NULL
);
